#ifdef DOXYGEN_SHOULD_SKIP_THIS
/**
   Tutorial for obtaining a Epsilon Grasp Quality from graspit API

   Modified from Jennifer Buehler's Graspit API

   Copyright (C) 2016 Pablo Frank

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
#endif   // DOXYGEN_SHOULD_SKIP_THIS

#include <grasp_planning_graspit/GraspItSceneManagerHeadless.h>
#include <grasp_planning_graspit/EigenGraspPlanner.h>
#include <grasp_planning_graspit/EigenGraspResult.h>
#include <string>
#include <vector>
#include "tinyxml.h"

///PFB for Qeps
#include <grasp_planning_graspit/GraspQualityEvaluator.h>

/**
 * \brief Tutorial on how to load a world and run the Eigengrasp planner.
 */
int main(int argc, char **argv)
{
  if (argc < 2)
  {
    std::cerr << "Usage: " << argv[0] << " <world-filename>" << std::endl;
    std::cerr << "OR " << std::endl;
    std::cerr << "Usage: " << argv[0] << " <object-filename> <robot-filename>" << std::endl;
    return 1;
  }

  // Create the graspit world manager.
  SHARED_PTR<GraspIt::GraspItSceneManager> graspitMgr(new GraspIt::GraspItSceneManagerHeadless());

  std::string worldFilename;
  std::string objectFilename;
  std::string robotFilename;

  if (argc < 3)
  {
    worldFilename = argv[1];
    if (worldFilename.empty())
    {
      std::cerr << "You have to specify a world" << std::endl;
      return 1;
    }

    // Load the graspit world
    graspitMgr->loadWorld(worldFilename);

  }
  else if (argc  == 3)
  {
    objectFilename = argv[1];
    robotFilename = argv[2];

    //Note
    //Eigen::Transform<double, 3, Eigen::Affine> EigenTransform;

    Eigen::AngleAxisd objT_aa(0.5*M_PI,Eigen::Vector3d::UnitX() );
    Eigen::AngleAxisd robT_aa(-0.5*M_PI,Eigen::Vector3d::UnitZ() );

    Eigen::Affine3d objT_aff(objT_aa);
    Eigen::Affine3d robT_aff(robT_aa);

    if ( graspitMgr->loadObject(objectFilename, "myObject", true, objT_aff) )
      std::cout<<"LOADED OBJECT"<<std::endl;
    if ( graspitMgr->loadRobot(robotFilename, "myRobot",robT_aff ) )
      std::cout<<"LOADED ROBOT"<<std::endl;

    graspitMgr->saveGraspItWorld("myTestLoadingNsaving.xml");
  }

  std::string name = "QualityEvaluator1";
  //Create the graspit accessor
  SHARED_PTR<GraspIt::GraspQualityEvaluator > graspitQev (new GraspIt::GraspQualityEvaluator(name, graspitMgr) );

  //! numHyperPlanes x 7 matrix of plane coefficients---> representing the resulting GWS
  //double **hyperPlanes;
  std::vector <std::vector <double> > hyperPlanes;
  //! number of 6D hyperplanes bounding this GWS
  int numHyperPlanes = 0;
  double qEps;

  // Default call has last three params default to true
  // double qEps = graspitQev->computeGWSandQuality(hyperplanes, &numHyperPlanes);

  //use default delay: 1500000
  qEps = graspitQev->computeGWSandQuality(hyperPlanes, numHyperPlanes,true, true, true);
  std::cout<< "resulting Epsilon Grasp Quality: "<< qEps <<std::endl;

  // uncomment for multiple tests
  /*
  int reps = 50;//was 50
      for (int ii = 0; ii < reps; ii++)
      {

        int delay = 500000 + ii*20000;
        //---------------------------------------------------------
        // a = withGravity,
        // b = withDynamics,
        // c = pauseDynamics
        //----------------------------------------------------------------   ,a  ,   b ,  c
        qEps = graspitQev->computeGWSandQuality(hyperPlanes, &numHyperPlanes,true, true, true,delay);
        std::cout<< ii <<", "<< delay<< ": resulting Epsilon Grasp Quality: "<< qEps <<std::endl;
      }
*/
  ///NOTE: the wrenches themselves are printed inside gws.cpp in int L1GWS::build(/**/)
  /*
  DBGP(" x: " << grasp->getContact(i)->getMate()->wrench[w].force.x() <<
       " y: " << grasp->getContact(i)->getMate()->wrench[w].force.y() <<
       " z: " << grasp->getContact(i)->getMate()->wrench[w].force.z() );
  DBGP("tx: " << grasp->getContact(i)->getMate()->wrench[w].torque.x() <<
       " ty: " << grasp->getContact(i)->getMate()->wrench[w].torque.y() <<
       " tz: " << grasp->getContact(i)->getMate()->wrench[w].torque.z() );

  std::cout<< "\n======================================= " <<std::endl;
  std::cout<< "resulting Epsilon Grasp Quality: "<< qEps <<std::endl;
  std::cout<< "=======================================\n\n " <<std::endl;
*/
  ///TODO: following doesn't quite work
  /// might need to use vector<vector<int> > &hyperPlanes instead
  /// and fill it inside GraspQualityEvaluayor::computeGWSandQuality

  // Printing the hyperplanes
  // -------------------------
  /// COMMENT OUT TO AVOID PRINTING HYPERPLANES
  if(1)
  {
    for(int ii = 0; ii< numHyperPlanes; ii++)
    {
      std::ostringstream oss;
      oss << ii <<": ";
      //double hp[7];
      for(int jj = 0; jj< 7; jj++)
      {
        //oss << hyperPlanes[ii][jj] << ", ";
        oss << hyperPlanes.at(ii).at(jj) << ", ";
        //hp[jj] = hyperPlanes[jj][ii];
      }

      std::cout<< std::setw(10) << oss.str()  << std::endl;
    }


  }

  return 0;
}
