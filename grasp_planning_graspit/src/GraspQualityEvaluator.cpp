/**
   Interface for accessing the GraspIt world for database management purposes.

   Copyright (C) 2016 Jennifer Buehler

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/


#include <grasp_planning_graspit/GraspQualityEvaluator.h>
#include <grasp_planning_graspit/LogBinding.h>
#include <robot.h>

#include <string>

///PFB for Qeps
#include <time.h>
#include "grasp.h"
#include "gws.h"
#include "gwsprojection.h"
#include "quality.h"
#include <fstream>
#include <unistd.h>

#define BILLION 1000000000L

using GraspIt::GraspQualityEvaluator;

GraspQualityEvaluator::GraspQualityEvaluator(const std::string& name, const SHARED_PTR<GraspItSceneManager>& interface):
  GraspItAccessor(name, interface)
{
  addAsIdleListener();
}

GraspQualityEvaluator::~GraspQualityEvaluator()
{
  PRINTMSG("GraspQualityEvaluator planner destructor");

  removeFromIdleListeners();

}

int GraspQualityEvaluator::loadRobotToWorld(const std::string& robotName, const EigenTransform& transform)
{
  Robot * robot = getRobotFromDatabase(robotName);
  if (!robot)
  {
    PRINTERROR("Robot " << robotName << " does not exist in database");
    return -1;
  }

  PRINTMSG("Adding robot...");
  int ret = addRobot(robot, transform);
  if (ret != 0)
  {
    PRINTERROR("Could not add robot to GraspIt world. Error code " << ret);
    return -2;
  }
  return 0;
}

int GraspQualityEvaluator::loadObjectToWorld(const std::string& objectName, const EigenTransform& transform)
{
  Body * object = getObjectFromDatabase(objectName);
  if (!object)
  {
    PRINTERROR("Object " << objectName << " does not exist in database");
    return -1;
  }

  PRINTMSG("Adding object...");
  int ret = addBody(object, transform);
  if (ret != 0)
  {
    PRINTERROR("Could not add object to GraspIt world. Error code " << ret);
    return -2;
  }
  return 0;
}

int GraspQualityEvaluator::loadToWorld(const int modelID, const EigenTransform& transform)
{
  int mType = getModelType(modelID);
  if (mType < 0)
  {
    PRINTERROR("Model " << modelID << " does not exist in database.");
    return -1;
  }
  if (mType == 1)  // is a robot
  {
    Robot * robot = getRobotFromDatabase(modelID);
    if (!robot)
    {
      PRINTERROR("Robot ID=" << modelID << " could not be retrieved.");
      return -1;
    }

    // PRINTMSG("Adding robot...");
    int ret = addRobot(robot, transform);
    if (ret != 0)
    {
      PRINTERROR("Could not add robot to GraspIt world. Error code " << ret);
      return -2;
    }
  }
  else     // is an object
  {
    Body * object = getObjectFromDatabase(modelID);
    if (!object)
    {
      PRINTERROR("Object ID=" << modelID << " could not be retrieved.");
      return -1;
    }

    // PRINTMSG("Adding object...");
    int ret = addBody(object, transform);
    if (ret != 0)
    {
      PRINTERROR("Could not add object to GraspIt world. Error code " << ret);
      return -2;
    }
  }
  return 0;
}


int GraspQualityEvaluator::unloadRobotFromWorld(const std::string& robotName)
{
  int id = -1;
  if (!getRobotModelID(robotName, id))
  {
    PRINTERROR("Robot " << robotName << " does not exist in database.");
    return -2;
  }
  return unloadFromWorld(id);
}

int GraspQualityEvaluator::unloadObjectFromWorld(const std::string& objectName)
{
  int id = -1;
  if (!getObjectModelID(objectName, id))
  {
    PRINTERROR("Object " << objectName << " does not exist in database.");
    return -2;
  }
  return unloadFromWorld(id);
}

int GraspQualityEvaluator::unloadFromWorld(const int modelID)
{
  int mType = getModelType(modelID);
  if (mType < 0)
  {
    PRINTERROR("Model " << modelID << " does not exist in database.");
    return -2;
  }
  WorldElement * elem = NULL;
  if (mType == 1)  // is a robot
  {
    Robot * robot = getRobotFromDatabase(modelID);
    if (!robot)
    {
      PRINTERROR("Robot ID=" << modelID << " could not be retrieved from the database.");
      return -2;
    }
    if (!isRobotLoaded(robot))
    {
      PRINTMSG("Robot " << modelID << " is not loaded GraspIt world.");
      return -1;
    }
    elem = robot;
    PRINTMSG("Removing robot " << modelID);
  }
  else   // is an object
  {
    Body * object = getObjectFromDatabase(modelID);
    if (!object)
    {
      PRINTERROR("Object ID=" << modelID << " could not be retrieved from the database.");
      return -2;
    }
    if (!isObjectLoaded(object))
    {
      PRINTMSG("Object " << modelID << " is not loaded GraspIt world.");
      return -1;
    }
    elem = object;
    PRINTMSG("Removing object " << modelID);
  }

  if (!removeElement(elem, false))
  {
    PRINTERROR("Could not remove model " << modelID << " from GraspIt world.");
    return -3;
  }
  return 0;
}





bool GraspQualityEvaluator::saveLoadedWorld(const std::string& filename, const bool asInventor, const bool createDir)
{
  if (asInventor)
  {
    if (!getGraspItSceneManager()->saveInventorWorld(filename, createDir))
    {
      PRINTERROR("Could not save inventor world in " << filename);
      if (!createDir) PRINTERROR("Does the directory exist?");
      return false;
    }
    // PRINTMSG("Saved inventor world in "<<filename);
  }
  else
  {
    if (!getGraspItSceneManager()->saveGraspItWorld(filename, createDir))
    {
      PRINTERROR("Could not save graspit world in " << filename);
      if (!createDir) PRINTERROR("Does the directory exist?");
      return false;
    }
    // PRINTMSG("Saved graspit world in "<<filename);
  }
  return true;
}


/**
 * Checks if this model is currently loaded in the graspit world
 * \retval 1 model is loaded
 * \retval 0 model is not loaded
 * \retval -1 model ID could not be found in database
 */
int GraspQualityEvaluator::isModelLoaded(const int modelID) const
{
  std::string name;
  bool isRobot;
  if (!getModelNameAndType(modelID, name, isRobot))
  {
    PRINTERROR("Could not find model " << modelID << " in database");
  }
  if (isRobot)
  {
    return readGraspItSceneManager()->isRobotLoaded(name) ? 1 : 0;
  }
  return readGraspItSceneManager()->isObjectLoaded(name) ? 1 : 0;
}

///PFB added for Qeps
void GraspQualityEvaluator::turnOnDynamycs()
{
  this->getGraspItSceneManager()->turnOnDynamycs();
}

///PFB added for Qeps
void GraspQualityEvaluator::turnOffDynamycs()
{
  this->getGraspItSceneManager()->turnOffDynamycs();
}

//double GraspQualityEvaluator::computeGWSandQuality(double **hyperPlanes_out,
//                                                   int &numHyperPlanes_out,
//                                                   bool withGravity,
//                                                   bool withDynamics,
//                                                   bool pauseDynamics,
//                                                   int delay
//                                                   )
double GraspQualityEvaluator::computeGWSandQuality(std::vector <std::vector <double> > & hyperPlanes_out,
                                                   int &numHyperPlanes_out,
                                                   bool withGravity,
                                                   bool withDynamics,
                                                   bool pauseDynamics,
                                                   int delay
                                                   )
{

  // timing
  // -------
  uint64_t diff;
  struct timespec start, end;
  clock_gettime(CLOCK_MONOTONIC, &start);	// mark start time

  // Initialize
  // ----------
  SHARED_PTR<GraspIt::GraspItSceneManager> graspitMgr = this->getGraspItSceneManager();
  //std::cout<<"Initializing from GraspQualityEvaluator: ";

  if (!graspitMgr->isInitialized())
  {
      graspitMgr->initialize();
  }
  //std::cout<<"Is Now Initialized"<<std::endl;

  // turn on dynamics
  // -----------------
  if(withDynamics){  graspitMgr->turnOnDynamycs(); }

  // AutoGrasp
  // ----------
  Grasp *grasp;
  GWS *gws;
  int count = 0;

  if (!  this->getCurrentHand()->autoGrasp(false, 1.0,false) )
  {
    PRINTWARN("Could not perform autograsp for grasp ");
    std::cout<<"Could not perform autograsp for grasp "<<std::endl;
    return -1;
  }

  grasp = this->getCurrentHand()->getGrasp();
  gws = grasp->addGWS("L1 Norm");

  // turn gravity on
  //------------------
  grasp->setGravity(withGravity);

  // setup for the Epsilon Quality
  // -----------------------
  QualEpsilon* qualEps = new QualEpsilon(grasp, QString("myEpsQuality"), gws);
  QualVolume* qualVol = new QualVolume(grasp, QString("myVolumeQuality"), gws);
  grasp->addQM(qualEps); // causes grasp->getNumQM() to return 1
  grasp->addQM(qualVol); // causes grasp->getNumQM() to return 2
  double epsQual3D;
  double volQual3D;

  do
  {
    count++;
    //std::cout<<"Waiting for complete grasp"<<std::endl;
    //sleep(1);
    unsigned int microseconds = delay;
    //std::cout<<"* * * * * delay: "<< delay <<std::endl;
    usleep(microseconds);

    // pause dynamics. THIS causes a grasp->update()
    // or call update for a 2nd pass
    // ---------------
    if(pauseDynamics && graspitMgr->dynamicsAreOn())
    {
      graspitMgr->turnOffDynamycs();
    }
    else
    {
      grasp->update();
    }

    //Get the Epsilon Quality
    // -----------------------
    if (grasp->getNumQM())
    {
      epsQual3D = qualEps->evaluate3D();
      volQual3D = qualVol->evaluate3D();
    }

    if (grasp->getNumContacts()>1 && (epsQual3D>0) && (volQual3D>0))
    {
      //std::cout<<"* * * * * GOOD GRASP, epsQual3D = "<<epsQual3D<<std::endl;
      break;
    }
  }while ( count < 5 );

  // return a stable "bad grasp" value
  if (epsQual3D < 0) {epsQual3D = -0.1;}

  // TIMING
  //---------
  clock_gettime(CLOCK_MONOTONIC, &end);	// mark the end time
  diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
  double diffSec= (double)diff/BILLION ;

  std::ostringstream oss;
//  oss<<grasp->getNumContacts()<<", "<<volQual3D << ", "<<epsQual3D<<", "<<gws->numHyperPlanes<<", "<<count<<", "<< diffSec<<"\n";
  oss<<diffSec<<", "<<volQual3D << ", "<<epsQual3D<<", "<<gws->numHyperPlanes<<", "<<count<<"\n";

//  std::cout<<"* * * * * Answer vector: nCOntacts, vol, eps, nHypPl, nAttempts, time\n";
//  std::cout << oss.str() << "\n"<<std::endl;

  // uncomment to save the result vector
  //------------------------------------
  std::ofstream outfile;
  outfile.open("api-graspit-grav-output-NEW.csv", std::ios_base::app);
  outfile << oss.str();

  // output
  //-------

  numHyperPlanes_out = gws->numHyperPlanes;

  //! 7 x numHyperPlanes matrix of plane coefficients
  double **gws_hyperPlanes;
  gws_hyperPlanes = gws->hyperPlanes;
//  hyperPlanes_out = gws->hyperPlanes;
//  hyperPlanes_out = new int *[7];
//  for(int i = 0; i <hyperPlanes_out; i++)
//      hyperPlanes_out[i] = new int[10];
  for(int ii = 0; ii < numHyperPlanes_out; ii++)
  {
    std::vector<double> hyperplane;
    for(int jj = 0; jj < 7; jj++)
    {
      hyperplane.push_back(gws_hyperPlanes[ii][jj]);
      //hyperPlanes_out.at<double>(ii,jj) = gws_hyperPlanes[ii][jj];
    }
    hyperPlanes_out.push_back(hyperplane);
  }

  /// COMMENT OUT TO AVOID PRINTING HYPERPLANES
  if(0)
  {
    for(int ii = 0; ii< numHyperPlanes_out; ii++)
    {
      std::ostringstream oss;
      oss << ii <<": ";
      //double hp[7];
      for(int jj = 0; jj< 7; jj++)
      {
        oss << hyperPlanes_out[ii][jj] << ", ";

      }

      std::cout<< std::setw(10) << oss.str()  << std::endl;
    }

    std::cout<<"\n*******************************"<<std::endl;
    std::cout<<"*******************************"<<std::endl;

  }
  return epsQual3D;
}

void GraspQualityEvaluator::idleEventFromSceneManager()
{
  std::cout<<" * * * * * Callback from main thread!"<<std::endl;
}
